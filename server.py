from flask import Flask, render_template, url_for, make_response, redirect, jsonify
import traceback

app = Flask(__name__)
app.config.update(DEBUG = False)


@app.route('/index.html', methods = ['GET'])
def landing_page():
    try:
        return render_template('index.html')
    except Exception as e:
        # returns an error if was any 
        return str(e)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'code': 404, 'error': 'Page not found', 'message': "{0}".format(error)}), 404)


# tracing the error in the caugth exception
def format_exception(e):
    exception_list = traceback.format_stack()
    exception_list = exception_list[:-2]
    exception_list.extend(traceback.format_tb(sys.exc_info()[2]))
    exception_list.extend(traceback.format_exception_only(sys.exc_info()[0], sys.exc_info()[1]))
    exception_str = "Traceback (most recent call last):\n"
    exception_str += "".join(exception_list)
    # Removing the last \n
    exception_str = exception_str[:-1]
    return exception_str


if __name__ == '__main__':
    app.run(host='0.0.0.0')
